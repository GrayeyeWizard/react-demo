import React from "react";
import PropTypes from "prop-types";

const Display = ({ notes, onDelete }) => {

  const handleDelete = (index) => {
    onDelete(index);
  };

  return (
    <ol>
      {notes.map(({FName, LName}, index) => (
        <>
          <li>
            Name: {FName} <br/>
            LastName: {LName} <br/>
            Index: {index}
          </li>
          <button onClick={()=>handleDelete(index)}>x</button>
        </>
      ))}
    </ol>
  );
};

Display.propTypes = {
  notes: PropTypes.array,
};

Display.defaultProps = {
  notes: [],
};

export default Display;
