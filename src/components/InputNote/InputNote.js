import React, {useState} from "react";

const InputNote = ({onSubmit}) => {
    const [FName, setFName] = useState('');
    const [LName, setLName] = useState('');

    const changeFName = (e) => setFName(e.target.value);
    const changeLName = (e) => setLName(e.target.value);
    const handleSubmit = () => {
        onSubmit({FName, LName});
        setFName('');
        setLName('');
    };

    return (
        <>
        <div>FirstName</div>
        <input type="text" onChange={changeFName} value={FName}></input><br/>
        <div>LastName</div>
        <input type="text" onChange={changeLName} value={LName}></input><br/>
        <button onClick={handleSubmit}>ADD</button>
        </>
    )
}

export default InputNote;