import React, {useState} from "react";
import Display from "../../components/Display/Display";
import InputNote from "../../components/InputNote/InputNote";

const Note = () => {
    const [notes, setNotes] = useState([]);
    const onSubmit = ({FName, LName}) => setNotes([...notes, {FName, LName}]);
    const onDelete = (i) => {
        const newNotes = notes.filter((item, index) => index !== i);
        setNotes(newNotes);
    }

    return (
        <>
        <InputNote onSubmit={onSubmit}></InputNote>
        <Display notes={notes} onDelete={onDelete}></Display>
        </>
    )
}

export default Note;